insert into station
values(10001, 'KHFI-FM', TRUE, 'KISS FM');

insert into station
values(10002, 'KPEZ', FALSE, '102.3 THE BEAT');

insert into station
values(10003, 'KKMJ-FM', TRUE, 'Majic 95.5');

insert into station
values(10004, 'KASE-FM', FALSE, 'KASE 101');