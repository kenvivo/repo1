package com.istations.demo;

import java.util.List;

public interface IStationDAO {
    List<Station> searchUser(List<SearchCriteria> params);

    void save(Station entity);
}
