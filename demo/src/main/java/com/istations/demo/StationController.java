package com.istations.demo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
@RestController
public class StationController {

    @Autowired
    private StationRepository stationRepository;

    @GetMapping("/stations")
    public List<Station> retrieveAllStations() {
            return stationRepository.findAll();
    }

    @RequestMapping("/findbyname")
    public String fetchDataByLastName(@RequestParam("name") String name){
        String result = "";

        for(Station cust: stationRepository.findByname(name)){
            result += cust.toString() + "</br>";
        }

        return result;
    }

    @RequestMapping(value = "/hdenabled", method = RequestMethod.GET)
    public List<Station> getAllActive() {
        return stationRepository.findByhdEnabled(true);
    }

    @GetMapping("/stations/{stationId}")
    public Station retrieveStation(@PathVariable long stationId) {
        Optional<Station> station = stationRepository.findById(stationId);

        if (!station.isPresent())
            throw new StationNotFoundException("stationId-" + stationId);

        return station.get();
    }

    @DeleteMapping("/stations/{stationId}")
    public void deleteStation(@PathVariable long stationId) {
            stationRepository.deleteById(stationId);
        }

    @PostMapping("/stations")
    public ResponseEntity<Object> createStation(@RequestBody Station station) {
        Station savedStation = stationRepository.save(station);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{stationId}")
            .buildAndExpand(savedStation.getStationId()).toUri();

        return ResponseEntity.created(location).build();

    }

    @PutMapping("/stations/{stationId}")
    public ResponseEntity<Object> updateStation(@RequestBody Station station, @PathVariable long stationId) {

        Optional<Station> stationOptional = stationRepository.findById(stationId);

        if (!stationOptional.isPresent())
            return ResponseEntity.notFound().build();

        station.setstationId(stationId);

        stationRepository.save(station);

        return ResponseEntity.noContent().build();
    }

    @Autowired
    private IStationDAO api;

    @RequestMapping(method = RequestMethod.GET, value = "/stations/search")
    @ResponseBody
    public List<Station> findAll(@RequestParam(value = "search", required = false) String search) {
        List<SearchCriteria> params = new ArrayList<SearchCriteria>();
        if (search != null) {
            Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                params.add(new SearchCriteria(matcher.group(1),
                        matcher.group(2), matcher.group(3)));
            }
        }
        return api.searchUser(params);

    }
}


