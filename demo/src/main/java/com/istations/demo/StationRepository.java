package com.istations.demo;

import com.istations.demo.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface StationRepository extends JpaRepository<Station, Long> {
//  List<Station> findByName(String name);

    List<Station> findByhdEnabled(boolean b);
    List<Station> findByname(String name);
}

