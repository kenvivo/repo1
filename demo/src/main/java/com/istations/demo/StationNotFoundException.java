package com.istations.demo;

public class StationNotFoundException extends RuntimeException{
    public StationNotFoundException(String exception) {
        super(exception);
    }
}
