package com.istations.demo;

import org.hibernate.search.annotations.*;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
@Indexed
@Getter
@Setter
//@Table(name = "STATION")
public class Station {

    //  @Column(name="STATION_ID")
    @Id
    @GeneratedValue
    private Long stationId;

    //@Column(name = "HD_ENABLED")
    @Field
    private Boolean hdEnabled;

    //  @NotBlank
    //  @Column(name="STATION_NAME", length = 150)
    @Field
    private String name;

    //    @NotBlank
    //    @Column(name="CALL_SIGN", length = 150)
    @Field
    private String callSign;

    public Station() {
        super();
    }

    public Station(Long stationId, Boolean hdEnabled, String name, String callSign) {
        this.stationId = stationId;
        this.hdEnabled = hdEnabled;
        this.name = name;
        this.callSign = callSign;
    }
    public Long stationId() {
        return stationId;
    }
    public void setstationId(Long stationId) {
        this.stationId = stationId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

//    public Boolean hdEnabled() {return hdEnabled;}
//    public void sethdEnabled(Boolean hdenabled) {
//        hdEnabled = hdenabled;
//    }
    //   public String getcallSign() {
    //       return callSign;
    //   }
    //  public void setcallSign(String callSign) {
    //     this.callSign = callSign;}
}
